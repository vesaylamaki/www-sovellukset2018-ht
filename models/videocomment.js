var mongoose = require('mongoose');
var moment = require('moment');

var Schema = mongoose.Schema;

// Create schema for videocomment
var VideocommentSchema = new Schema(
  {
    video: { type: Schema.Types.ObjectId, ref: 'Video', required: true },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    comment: { type: String, required: true },
    date: { type: Date, default: Date.now }
  },
  {
    // Allow virtual attributes included in object/json formats
    toObject:{ virtuals:true },
    toJSON:{ virtuals:true }
  }
);

// Format date to timestamp
VideocommentSchema
.virtual('timestamp')
.get(function () {
  return this.date ? moment(this.date).format('D.M.YYYY HH:mm') : '';
});

//Export model
module.exports = mongoose.model('Videocomment', VideocommentSchema);
