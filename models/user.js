// load libraries
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema user model
var userSchema = mongoose.Schema({

  // Admin users can access more features
  admin: { type: Boolean, default: false },

  // Account can have several authentication methods
  // (only local is used now, rest are for future developement)
  local: {
    username: String,
    password: String,
  },
  facebook: {
    id: String,
    token: String,
    name: String,
    email: String
  },
  twitter: {
    id: String,
    token: String,
    displayName: String,
    username: String
  },
  google: {
    id: String,
    token: String,
    email: String,
    name: String
  }

});

// methods
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to app
module.exports = mongoose.model('User', userSchema);
