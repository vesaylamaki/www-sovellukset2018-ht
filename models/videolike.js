var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// Create schema for videolike
var VideolikeSchema = new Schema(
  {
    video: { type: Schema.Types.ObjectId, ref: 'Video', required: true },
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true }
  }
);

//Export model
module.exports = mongoose.model('Videolike', VideolikeSchema);
