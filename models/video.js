var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// Create schema for video
var VideoSchema = new Schema(
  {
    title: {type: String, required: true},
    url: {type: String, required: true}
  }
);

//Export model
module.exports = mongoose.model('Video', VideoSchema);
