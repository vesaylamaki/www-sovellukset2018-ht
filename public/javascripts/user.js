"use strict"; // executed in 'strict mode'

// Component used to render comments
Vue.component('comment-component', {
  props: ['comment', 'index'],
  computed: {
    id: function() {
      return this.comment._id
    },
    user: function() {
      return this.comment.user.local.username
    },
    message: function() {
      return this.comment.comment
    },
    timestamp: function() {
      return this.comment.timestamp
    }
  },
  template: `
    <li class="collection-item row grey darken-4 grey-text text-lighten-3">
      <div class="col s4">
        <div class="row">
          <div class="col s12 flow-text">{{ this.user }}: </div>
          <div class="col s12 flow-text timestamp">{{ this.timestamp }}</div>
        </div>
      </div>
      <div class="col s8 flow-text">{{ this.message }}</div>
    </li>
    `
})

// Component used to render comment list
Vue.component('commentlist-component', {
  props: ['video'],
  data: function() {
    return {
      comments: [],
      newComment: '',
      errors: []
    }
  },
  methods: {
    fetchComments: function() {
      var path = '/videos/'+this.video+'/comments'
      axios
        .get(path)
        .then(response => (this.comments = response.data.comments))
        .catch(error => { console.log(error) })
    },
    addComment: function() {
      var path = '/videos/'+this.video+'/comment';
      if (!this.validateComment()) {
        return;
      }
      axios
       .post(path, {
         video: this.video,
         comment: this.newComment
        })
       .then(response => {
         this.comments.push(response.data);
         this.newComment = '';
       })
       .catch(error => {
         console.log(error);
       });
    },
    validateComment: function() {
      this.errors = [];
      var maxLength = 5000;

      if (this.newComment === '') {
        return false;
      }
      if (this.newComment.length > maxLength) {
        this.errors.push('Maximum length for comment is ' + maxLength + ' characters.');
      }

      if (!this.errors.length) {
        return true;
      }
      return false;
    }
  },
  mounted() {
    this.fetchComments();
  },
  template: `
    <div>
      <div class="row">
        <div class="col s12 grey darken-4 grey-text text-lighten-3 center flow-text">Comments</div>
        <ul class="collection col s12" v-if='this.comments.length'>
          <comment-component
            v-for="(comment, index) in this.comments"
            v-bind:key="index"
            v-bind:comment="comment"
            v-bind:index="index"></comment-component>
        </ul>
        <div class="col s12 grey darken-4 grey-text text-lighten-3 center flow-text" v-if='!this.comments.length'>No comments</div>
      </div>
      <form class="row">
        <div class="input-field col s10 m8 l6 offset-s1 offset-m2 offset-l3">
          <input class="validate grey-text text-lighten-3" type="text" v-model="newComment"></input>
        </div>
        <a class="waves-effect waves-light btn col s10 m8 l6 offset-s1 offset-m2 offset-l3" v-on:click="this.addComment">Comment</a>
      </form>
      <div class="row" v-if="this.errors.length">
        <div class="error-message col s10 offset-s1 center flow-text" v-for="error in errors">{{ error }}</div>
      </div>
    </div>
    `
})

// Component used to render videos
Vue.component('video-component', {
  props: ['info', 'index'],
  data: function() {
    return {
      likes: 0,
      userlike: false,
      videolikeid: ''
    }
  },
  computed: {
    id: function() {
      return this.info._id
    },
    title: function() {
      return this.info.title
    },
    code: function() {
      return this.info.url
    }
  },
  methods: {
    fetchInfo: function() {
      axios
        .get('/videos/'+this.id)
        .then(response => {
          this.likes = response.data.likes;
          // check if current user has liked this video
          if(response.data.userlike.length)
            this.userlike = true;
            this.videolikeid = response.data.userlike._id;
        })
        .catch(error => { console.log(error) })
    },
    toggleLike: function() {
      var path;
      if(this.userlike) {
        path = '/videos/dislike';
        this.likes -= 1;
      }
      else {
        path = '/videos/like';
        this.likes += 1;
      }
      this.userlike = !this.userlike;
      axios
       .post(path, {
         video: this.id,
         videolikeid: this.videolikeid
        })
       .then(response => {
         this.videolikeid = response.data.videolikeid;
       })
       .catch(error => {
         console.log(error);
         this.userlike = !this.userlike;
       });
    }
  },
  mounted() {
    this.fetchInfo();
  },
  template: `
    <li class="collection-item grey darken-4 grey-text text-lighten-3">
      <div class="row">
        <h4 class="col s12 flow-text">{{ this.title }}</h4>
      </div>
      <div class="row video-container">
        <iframe
          width="560"
          height="315"
          v-bind:src="'https://www.youtube.com/embed/'+this.code+'?controls=0'"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen></iframe>
      </div>
      <div class="row valign-wrapper">
        <a class="btn-flat col s2 red-text text-darken-4 right-align flow-text" v-on:click="toggleLike">
          <i class="material-icons" v-if='this.userlike'>favorite</i>
          <i class="material-icons" v-else>favorite_border</i>
        </a>
        <div class="col s10 flow-text">Likes: {{ this.likes }}</div>
      </div>
      <div class="row">
        <commentlist-component class="col s12 m10 l8 offset-m1 offset-l2"
          v-bind:video="this.id"></commentlist-component>
      </div
    </li>
    `
})
