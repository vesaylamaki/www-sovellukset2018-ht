"use strict"; // executed in 'strict mode'

// Component used to render user
Vue.component('user-component', {
  props: ['user', 'index'],
  data: function() {
    return {
      newAdmin: false
    }
  },
  computed: {
    oldAdmin: function() {
      return this.user.admin
    }
  },
  methods: {
    updateUser: function() {
      var path = '/users/'+this.user._id+'/update';
      if(this.newAdmin !== this.oldAdmin) {
        axios
         .post(path, {
           admin: this.newAdmin,
           user: this.user._id
          })
         .then(response => {
           // console.log(response.data);
         })
         .catch(error => {
           console.log(error);
         });
       }
    }
  },
  mounted() {
    // initialize newAdmin value
    this.newAdmin = this.oldAdmin;
  },
  template: `
    <li class="collection-item row grey darken-4 grey-text text-lighten-3 valign-wrapper">
      <div class="col s1 flow-text">{{ index + 1 }}</div>
      <div class="col s5 flow-text">{{ user.local.username }}</div>
      <p class="col s4 flow-text"><label>
        <input v-if="oldAdmin" id="checkbox" type="checkbox" v-model="newAdmin" checked="checked"></input>
        <input v-else id="checkbox" type="checkbox" v-model="newAdmin"></input>
        <span>{{ newAdmin }}</span>
      </label></p>
      <a class="col s1 waves-effect waves-teal btn-flat center-align flow-text" v-on:click="updateUser">
        <i class="material-icons grey-text text-lighten-3">update</i>
      </a>
      <a class="col s1 waves-effect waves-teal btn-flat center-align flow-text" v-on:click="$emit('delete-user')">
        <i class="material-icons grey-text text-lighten-3">delete</i>
      </a>
    </li>
    `
})

// Component used to render user list
Vue.component('userlist-component', {
  data: function() {
    return {
      users: []
    }
  },
  methods: {
    fetchUsers: function() {
      axios
        .get('/users/list')
        .then(response => (this.users = response.data))
        .catch(error => { console.log(error) })
    },
    deleteUser: function(index) {
      var path = '/users/'+this.users[index]._id+'/delete';
      axios
       .post(path, {
         user: this.users[index]._id
        })
       .then(response => {
         this.users.splice(index, 1)
       })
       .catch(error => {
         console.log(error);
       });
    }
  },
  mounted() {
    this.fetchUsers();
  },
  template: `
    <div>
      <ul class="collection col s12 m10 l8 offset-m1 offset-l2" v-if='this.users.length'>
        <li class="collection-item row grey darken-4 grey-text text-lighten-3 valign-wrapper">
          <div class="col s5 offset-s1">Username</div>
          <div class="col s6">is Admin</div>
        </li>
        <user-component
          v-for="(user, index) in this.users"
          v-bind:key="index"
          v-bind:user="user"
          v-bind:index="index"
          v-on:delete-user="deleteUser(index)"></user-component>
      </ul>
      <div class="col s12 m10 l8 offset-m1 offset-l2 flow-text center" v-if='!this.users.length'> No users :(</div>
    </div>
    `
})

// Component used to render comment list
Vue.component('commentlist-component', {
  props: ['video'],
  data: function() {
    return {
      comments: [],
      newComment: '',
      errors: []
    }
  },
  methods: {
    fetchComments: function() {
      var path = '/videos/'+this.video+'/comments'
      axios
        .get(path)
        .then(response => (this.comments = response.data.comments))
        .catch(error => { console.log(error) })
    },
    addComment: function() {
      var path = '/videos/'+this.video+'/comment';
      axios
       .post(path, {
         video: this.video,
         comment: this.newComment
        })
       .then(response => {
         this.comments.push(response.data);
         this.newComment = '';
       })
       .catch(error => {
         console.log(error);
       });
    },
    validateComment: function() {
      this.errors = [];
      var maxLength = 5000;

      if (this.newComment === '') {
        return false;
      }
      if (this.newComment.length > maxLength) {
        this.errors.push('Maximum length for comment is ' + maxLength + ' characters.');
      }

      if (!this.errors.length) {
        return true;
      }
      return false;
    },
    deleteComment: function(index) {
      var path = '/videos/'+this.video+'/comment/delete';
      axios
       .post(path, {
         comment: this.comments[index]._id
        })
       .then(response => {
         this.comments.splice(index, 1)
       })
       .catch(error => {
         console.log(error);
       });
    }
  },
  mounted() {
    this.fetchComments();
  },
  template: `
    <div>
      <div class="row">
        <div class="col s12 grey darken-4 grey-text text-lighten-3 flow-text">Comments</div>
        <ul class="collection col s12" v-if='this.comments.length'>
          <comment-component
            v-for="(comment, index) in this.comments"
            v-bind:key="index"
            v-bind:comment="comment"
            v-bind:index="index"
            v-on:delete-comment="deleteComment(index)"></comment-component>
        </ul>
        <div class="col s12 grey darken-4 grey-text text-lighten-3 flow-text" v-if='!this.comments.length'>No comments</div>
      </div>
      <form class="row">
        <div class="input-field col s10 m8 l6 offset-s1 offset-m2 offset-l3">
          <input class="validate grey-text text-lighten-3" type="text" v-model="newComment"></input>
        </div>
        <a class="waves-effect waves-light btn col s10 m8 l6 offset-s1 offset-m2 offset-l3" v-on:click="this.addComment">Comment</a>
      </form>
      <div class="row" v-if="this.errors.length">
        <div class="error-message col s10 offset-s1 center flow-text" v-for="error in errors">{{ error }}</div>
      </div>
    </div>
    `
})

// Component used to render comment
Vue.component('comment-component', {
  props: ['comment', 'index'],
  computed: {
    id: function() {
      return this.comment._id
    },
    user: function() {
      return this.comment.user
    },
    message: function() {
      return this.comment.comment
    },
    timestamp: function() {
      return this.comment.timestamp
    }
  },
  template: `
    <li class="collection-item row grey darken-4 grey-text text-lighten-3">
      <div class="col s4">
        <div class="row">
          <div class="col s12 flow-text">{{ this.user.local.username }}: </div>
          <div class="col s12 flow-text timestamp">{{ this.timestamp }}</div>
          <a class="waves-effect waves-teal btn-flat white-text col s12 left flow-text" v-on:click="$emit('delete-comment')">
            <i class="material-icons">delete</i>
          </a>
        </div>
      </div>
      <div class="col s8 flow-text">{{ this.message }}</div>
    </li>
    `
})

// Component used to render video list
Vue.component('video-component', {
  props: ['info', 'index'],
  data: function() {
    return {
      likes: 0,
      userlike: false,
      videolikeid: ''
    }
  },
  computed: {
    id: function() {
      return this.info._id
    },
    title: function() {
      return this.info.title
    },
    code: function() {
      return this.info.url
    }
  },
  methods: {
    fetchInfo: function() {
      axios
        .get('/videos/'+this.id)
        .then(response => {
          this.likes = response.data.likes;
          if(response.data.userlike.length)
            this.userlike = true;
            this.videolikeid = response.data.userlike._id;
        })
        .catch(error => { console.log(error) })
    },
    toggleLike: function() {
      var path;
      if(this.userlike) {
        path = '/videos/dislike';
        this.likes -= 1;
      }
      else {
        path = '/videos/like';
        this.likes += 1;
      }
      this.userlike = !this.userlike;
      axios
       .post(path, {
         video: this.id,
         videolikeid: this.videolikeid
        })
       .then(response => {
         this.videolikeid = response.data.videolikeid;
       })
       .catch(error => {
         console.log(error);
         this.userlike = !this.userlike;
       });
    },
    deleteVideo: function(index) {
      var path = '/videos/'+this.video+'/delete';
      axios
       .post(path, {
         video: this.video,
         comment: this.comments[index]._id
        })
       .then(response => {
         this.comments.splice(index, 1)
       })
       .catch(error => {
         console.log(error);
       });
    }
  },
  mounted() {
    this.fetchInfo();
  },
  template: `
    <li class="collection-item grey darken-4 grey-text text-lighten-3">
      <div class="row">
        <h4 class="col s12 flow-text">{{ this.title }}</h4>
      </div>
      <div class="row video-container">
        <iframe
          width="560"
          height="315"
          v-bind:src="'https://www.youtube.com/embed/'+this.code"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen></iframe>
      </div>
      <div class="row valign-wrapper">
        <a class="btn-flat col s2 red-text text-darken-4 right-align flow-text" v-on:click="toggleLike">
          <i class="material-icons" v-if='this.userlike'>favorite</i>
          <i class="material-icons" v-else>favorite_border</i>
        </a>
        <div class="col s8">{{ this.likes }}</div>
        <a class="waves-effect waves-teal btn-flat white-text col s2 left" v-on:click="$emit('delete-video')">
          <i class="material-icons">delete</i>
        </a>
      </div>
      <div class="row">
        <commentlist-component class="col s12 m10 l8 offset-m1 offset-l2"
          v-bind:video="this.id"></commentlist-component>
      </div
    </li>
    `
})
