"use strict"; // executed in 'strict mode'

// Run after page is loaded
document.addEventListener('DOMContentLoaded', function() {

  // Initialize Materialize components
  M.AutoInit();

  // Vue object
  new Vue({

    el: '#container', // target #container element

    data() {
      return {
        videos: [],
        newVideoTitle: '',
        newVideoCode: '',
        errors: []
      };
    },

    mounted() {
      // load videos in videos-array
      axios
        .get('/videos')
        .then(response => (this.videos = response.data))
        .catch(error => { console.log(error) })
    },

    methods: {

      sendNewVideo() {
        if (!this.validateVideoPost()) {
          return;
        }
        axios
        .post('/videos/new', {
          title: this.newVideoTitle,
          url: this.newVideoCode
        })
        .then(response => (this.videos.push(response.data))) // add new video in array
        .catch(error => { console.log(error) });
        // Set text fields empty
        this.newVideoTitle = '';
        this.newVideoCode = '';
      },

      deleteVideo(index) {
        var path = '/videos/'+this.videos[index]._id+'/delete';
        axios
         .post(path, {
           videoid: this.videos[index]._id
          })
         .then(response => {
           this.videos.splice(index, 1) // remove deleted video from array
         })
         .catch(error => {
           console.log(error);
         });
      },

      validateVideoPost() {
        this.errors = [];
        var maxLength = 50; // maximum length of video title
        var codeLength = 11; // Youtube video code length
        var alphaNum = /[^a-zA-Z0-9]/; // contains letters and numbers

        // Check text fields
        if (this.newVideoTitle === '') {
          this.errors.push('Title is required.');
        }
        if (this.newVideoTitle.length > maxLength) {
          this.errors.push('Maximum length for title is ' + maxLength + ' characters.');
        }
        if (alphaNum.test(this.newVideoCode) || this.newVideoCode.length != codeLength) {
          this.errors.push('Invalid url.');
        }

        if (!this.errors.length) {
          return true;
        }
        return false;
      },
    }

  })

  // Initialize materialize tabs
  var tabs = document.querySelectorAll('.tabs')
  for (var i = 0; i < tabs.length; i++){
  	M.Tabs.init(tabs[i]);
  }

});
