"use strict"; // executed in 'strict mode'

// Component used to render video list
Vue.component('video-component', {
  props: ['info', 'index'],
  data: function() {
    return {
      likes: 0
    }
  },
  computed: {
    id: function() {
      return this.info._id
    },
    title: function() {
      return this.info.title
    },
    code: function() {
      return this.info.url
    }
  },
  methods: {
    fetchInfo: function() {
      // load video likes
      axios
        .get('/videos/'+this.id)
        .then(response => {
          this.likes = response.data.likes;
        })
        .catch(error => { console.log(error) })
    }
  },
  mounted() {
    this.fetchInfo();
  },
  template: `
  <li class="collection-item grey darken-4 grey-text text-lighten-3">
    <div class="row">
      <h4 class="col s12 flow-text">{{ this.title }}</h4>
    </div>
    <div class="row video-container">
      <iframe
        width="560"
        height="315"
        v-bind:src="'https://www.youtube.com/embed/'+this.code+'?controls=0'"
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen></iframe>
    </div>
    <div class="row">
      <div class="col s10 offset-s2 flow-text">Likes: {{ this.likes }}</div>
    </div>
    <div class="row">
      <commentlist-component class="col s12 m10 l8 offset-m1 offset-l2"
        v-bind:video="this.id"></commentlist-component>
    </div
  </li>
    `
})

// Component used to render comment list
Vue.component('commentlist-component', {
  props: ['video'],
  data: function() {
    return {
      comments: []
    }
  },
  methods: {
    fetchComments: function() {
      var path = '/videos/'+this.video+'/comments'
      axios
        .get(path)
        .then(response => (this.comments = response.data.comments))
        .catch(error => { console.log(error) })
    }
  },
  mounted() {
    this.fetchComments();
  },
  template: `
    <div class="row">
      <h5 class="col s12 grey darken-4 grey-text text-lighten-3 center flow-text">Comments</h5>
      <ul class="collection col s12" v-if='this.comments.length'>
        <comment-component
          v-for="(comment, index) in this.comments"
          v-bind:key="index"
          v-bind:comment="comment"
          v-bind:index="index"></comment-component>
      </ul>
      <div class="col s12 grey darken-4 grey-text text-lighten-3 center flow-text" v-if='!this.comments.length'>No comments</div>
    </div>
    `
})

// Component used to render video list
Vue.component('comment-component', {
  props: ['comment', 'index'],
  computed: {
    id: function() {
      return this.comment._id
    },
    user: function() {
      return this.comment.user.local.username
    },
    message: function() {
      return this.comment.comment
    },
    timestamp: function() {
      return this.comment.timestamp
    }
  },
  template: `
    <li class="collection-item row grey darken-4 grey-text text-lighten-3">
      <div class="col s4">
        <div class="row">
          <div class="col s12 flow-text">{{ this.user }}: </div>
          <div class="col s12 flow-text timestamp">{{ this.timestamp }}</div>
        </div>
      </div>
      <div class="col s8 flow-text">{{ this.message }}</div>
    </li>
    `
})
