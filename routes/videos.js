var express = require('express');
var router = express.Router();

// Load controllers
var auth_controller = require('../controllers/authenticationController');
var video_controller = require('../controllers/videoController');

// GET video list
router.get('/', video_controller.video_list);

// POST new video
router.post('/new', auth_controller.isUser, auth_controller.isAdmin, video_controller.video_create);

// POST videolike
router.post('/like', auth_controller.isUser, video_controller.videolike_create);

// POST videolike delete
router.post('/dislike', auth_controller.isUser, video_controller.videolike_delete);

// GET video info
router.get('/:id', video_controller.video_info);

// POST video delete
router.post('/:id/delete', video_controller.video_delete);

// GET video comments
router.get('/:id/comments', video_controller.video_comments);

// POST new comment
router.post('/:id/comment', auth_controller.isUser, video_controller.videocomment_create);

// POST delete comment
router.post('/:id/comment/delete', auth_controller.isUser, video_controller.videocomment_delete);

module.exports = router;
