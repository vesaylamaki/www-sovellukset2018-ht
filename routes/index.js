var express = require('express');
var router = express.Router();

// Load controllers
var video_controller = require('../controllers/videoController');
var auth_controller = require('../controllers/authenticationController');
var nav_controller = require('../controllers/navigationController');

// GET visitor home page
router.get('/', nav_controller.visitor_page);

// GET users page
router.get('/user', auth_controller.isUser, nav_controller.user_page);

// GET admins page
router.get('/admin', auth_controller.isUser, auth_controller.isAdmin, nav_controller.admin_page);

// GET login page
router.get('/login', nav_controller.login_page);

// process the login form
router.post('/login', auth_controller.login_form);

// GET signup page
router.get('/signup', nav_controller.signup_page);

// process the signup form
router.post('/signup', auth_controller.signup_form);

// GET logout
router.get('/logout', auth_controller.isUser, auth_controller.logout);


module.exports = router;
