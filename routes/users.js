var express = require('express');
var router = express.Router();

// Load controllers
var auth_controller = require('../controllers/authenticationController');
var user_controller = require('../controllers/userController');

// GET user list
router.get('/list', auth_controller.isUser, auth_controller.isAdmin, user_controller.user_list);

// POST user delete
router.post('/:id/delete', auth_controller.isUser, auth_controller.isAdmin, user_controller.user_delete);

// POST user update
router.post('/:id/update', auth_controller.isUser, auth_controller.isAdmin, user_controller.user_update);

module.exports = router;
