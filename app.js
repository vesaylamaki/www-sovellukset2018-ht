// Load modules
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');

// Initialize app
var app = express();

// Set up mongoose connection
var mongoose = require('mongoose');
var configDB = require('./config/database.js');
mongoose.connect(configDB.docker); // Load db url from config
mongoose.Promise = global.Promise; // Get Mongoose to use the global promise library
var db = mongoose.connection; // Get the default connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// Passport configuration
require('./config/passport')(passport);

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// App configuration
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Session setup
app.use(session({
    secret: 'session-secret',
    cookie: { maxAge: 60000 },
    resave: false,
    saveUninitialized: false
  }));

// Passport setup
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// Routes
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var videosRouter = require('./routes/videos');
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/videos', videosRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
