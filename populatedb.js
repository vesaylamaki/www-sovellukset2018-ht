#! /usr/bin/env node

'use strict';

console.log('This script populates some info to database');

// Load libraries and models
var async = require('async')
var User = require('./models/user')
var Video = require('./models/video')
var Videocomment = require('./models/videocomment')
var Videolike = require('./models/videolike')

// Set up mongoose connection
var mongoose = require('mongoose');
var configDB = require('./config/database.js');
mongoose.connect(configDB.docker);
mongoose.Promise = global.Promise; // Get Mongoose to use the global promise library
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// List variables for new db items
var users = []
var videos = []
var videocomments = []
var videolikes = []

// Functions to create db instances

function userCreate(username, password, admin, cb) {
  var userdetail = {
    local: {
      username: username
    },
    admin: admin
  }

  var user = new User(userdetail);
  user.local.password = user.generateHash(password);

  user.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('New User: ' + user);
    users.push(user)
    cb(null, user)
  }  );
}

function videoCreate(title, url, cb) {
  var videodetail = {
    title: title,
    url: url
  }

  var video = new Video(videodetail);

  video.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('NewVideo: ' + video);
    videos.push(video)
    cb(null, video)
  }  );
}

function videocommentCreate(video, user, comment, cb) {
  var videocommentdetail = {
    video: video._id,
    user: user._id,
    comment: comment
  }

  var videocomment = new Videocomment(videocommentdetail);

  videocomment.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('NewVideocomment: ' + videocomment);
    videocomments.push(videocomment)
    cb(null, videocomment)
  }  );
}

function videolikeCreate(video, user, cb) {
  var videolikedetail = {
    video: video._id,
    user: user._id
  }

  var videolike = new Videolike(videolikedetail);

  videolike.save(function (err) {
    if (err) {
      cb(err, null)
      return
    }
    console.log('NewVideolike: ' + videolike);
    videolikes.push(videolike)
    cb(null, videolike)
  }  );
}

// Functions to populate database with some data

function createUsersVideos(cb) {
    async.parallel([
        function(callback) {
          userCreate('admin', 'admin', true, callback);
        },
        function(callback) {
          userCreate('Matti', 'teppo', true, callback);
        },
        function(callback) {
          userCreate('user', 'user', false, callback);
        },
        function(callback) {
          userCreate('Teppo', 'matti', false, callback);
        },
        function(callback) {
          videoCreate('Mikä olet', 'PWFh1HPgbD0', callback);
        },
        function(callback) {
          videoCreate('Lapin Leenuska', 'qNm7m3w0jLw', callback);
        },
        function(callback) {
          videoCreate('Saatana saapuu Mattilaan (live)', 'HUVKDvvtvik', callback);
        },
        function(callback) {
          videoCreate('Hiellä ja työllä', '52zJkUetcSY', callback);
        },
        ],
        // optional callback
        cb);
}

function createVideocomments(cb) {
    async.parallel([
        function(callback) {
          videocommentCreate(videos[0], users[0], 'Tätä ei voi pitää taiteena!', callback);
        },
        function(callback) {
          videocommentCreate(videos[0], users[2], 'Mikä näitä ihmisiä vaivaa...?.', callback);
        },
        function(callback) {
          videocommentCreate(videos[1], users[1], 'Eka', callback);
        },
        function(callback) {
          videocommentCreate(videos[1], users[3], 'Toka', callback);
        },
        function(callback) {
          videocommentCreate(videos[1], users[0], 'Kommenttikenttä ei ole tarkoitettu asiattomuuksille. Tästä eteenpäin asiattomat viestit poistetaan ylläpidon toimesta, ja häirikön käyttäjätunnus lukitaan kuukaudeksi.', callback);
        },
        function(callback) {
          videocommentCreate(videos[3], users[2], 'Tätä voi pitää taiteena!', callback);
        },
        function(callback) {
          videocommentCreate(videos[3], users[0], 'Uskoo ken haluaa..', callback);
        }
        ],
        // optional callback
        cb);
}

function createVideolikes(cb) {
    async.parallel([
        function(callback) {
          videolikeCreate(videos[0], users[0], callback)
        },
        function(callback) {
          videolikeCreate(videos[1], users[0], callback)
        },
        function(callback) {
          videolikeCreate(videos[2], users[0], callback)
        },
        function(callback) {
          videolikeCreate(videos[3], users[0], callback)
        },
        function(callback) {
          videolikeCreate(videos[0], users[1], callback)
        },
        function(callback) {
          videolikeCreate(videos[2], users[1], callback)
        },
        function(callback) {
          videolikeCreate(videos[3], users[1], callback)
        },
        function(callback) {
          videolikeCreate(videos[2], users[2], callback)
        },
        function(callback) {
          videolikeCreate(videos[3], users[2], callback)
        },
        function(callback) {
          videolikeCreate(videos[0], users[3], callback)
        },
        function(callback) {
          videolikeCreate(videos[1], users[3], callback)
        }
        ],
        // Optional callback
        cb);
}

// Run the population functions
async.series([
    createUsersVideos,
    createVideocomments,
    createVideolikes
],
// Optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
    else {
        console.log('RESULTS: '+results);
    }
    // All done, disconnect from database
    mongoose.connection.close();
});
