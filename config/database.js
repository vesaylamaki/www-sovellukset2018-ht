module.exports = {
    // database paths
    'local': 'mongodb://127.0.0.1/ketjis',
    'docker': 'mongodb://mongo:27017/ketjis'
};
