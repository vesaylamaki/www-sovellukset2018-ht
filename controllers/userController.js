// load models
var User = require('../models/user');
var Videolike = require('../models/videolike');
var Videocomment = require('../models/videocomment');

// Handle 
exports.user_list = function(req, res, next) {
  User.find()
    .exec(function (err, user_list) {
      if (err) { return next(err); }
      //Successful, so respond
      res.json(user_list);
    });
};

// Handle User delete on POST.
exports.user_delete = function(req, res, next) {
    // Delete object
    User.findOneAndDelete({ _id: req.body.user }, function deleteUser(err, user) {
        if (err) { return next(err); }
        // Success
        Videolike.deleteMany({ user: user._id }, function (err) {});
        Videocomment.deleteMany({ user: user._id }, function (err) {});
        res.json({ user: 'deleted' });
    })
};

// Handle User update on POST.
exports.user_update = function(req, res, next) {
    // Update object
    User.findOneAndUpdate({ _id: req.body.user }, { admin: req.body.admin }, function deleteUser(err, user) {
        if (err) { return next(err); }
        res.json(user);
    })
};
