var passport = require('passport');

// login form handler
exports.login_form = function(req, res, next) {
  passport.authenticate('local-login', {
    successRedirect : '/admin', // redirect to the admin section
    failureRedirect : '/login', // redirect back to the login page if there is an error
    failureFlash : true // allow flash messages
  })(req, res, next);
};

// signup form handler
exports.signup_form = function(req, res, next) {
  passport.authenticate('local-signup', {
    successRedirect : '/admin', // redirect to the admin section
    failureRedirect : '/signup', // redirect back to the signup page if there is an error
    failureFlash : true // allow flash messages
  })(req, res, next);
};

// route middleware to make sure a user is logged in
exports.isUser = function(req, res, next) {
  // if user is authenticated in the session, carry on
  if (req.isAuthenticated())
    return next();
  // if they aren't redirect them to the home page
  res.redirect('/');
}

// route middleware to make sure a user logged in is admin
exports.isAdmin = function(req, res, next) {
  // if admin user is authenticated in the session, carry on
  if (req.user.admin)
    return next();
  // if they aren't redirect them to the user page
  res.redirect('/user');
}

// logout
exports.logout = function(req, res) {
    req.logout();
    res.redirect('/');
};
