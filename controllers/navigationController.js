// Rendered pages

// visitor home page
exports.visitor_page = function(req, res, next) {
  res.render('index', {
    title: 'Ketjuerektio'
  });
};

// user home Page
exports.user_page = function(req, res, next) {
    res.render('users', {
      title: 'Ketjuerektio',
      user : req.user // get the user out of session and pass to template
    });
};

// admin home Page
exports.admin_page = function(req, res, next) {
    res.render('admins', {
      title: 'Ketjuerektio',
      user : req.user // get the user out of session and pass to template
    });
};

// login Page
exports.login_page = function(req, res, next) {
  // render the page and pass in any flash data if it exists
  res.render('login', {
    title: 'Log In',
    message: req.flash('loginMessage')
  });
};

// sign up page
exports.signup_page = function(req, res) {
    // render the page and pass in any flash data if it exists
    res.render('signup', {
      title: 'Sign Up',
      message: req.flash('signupMessage')
    });
};
