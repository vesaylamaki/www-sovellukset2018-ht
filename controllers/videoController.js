var Video = require('../models/video');
var Videolike = require('../models/videolike');
var Videocomment = require('../models/videocomment');

var async = require('async');

const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Get list of all videos
exports.video_list = function(req, res, next) {

  Video.find()
    .exec(function (err, video_list) {
      if (err) { return next(err); }
      //Successful, so respond
      res.json(video_list);
    });

};

// Handle Video create on POST.
exports.video_create = [

    // Validate fields
    body('title', 'Title required').isLength({ min: 1 }).trim(),
    body('url', 'Url required').isLength({ min: 1 }).trim(),

    // Sanitize fields
    sanitizeBody('title').trim().escape(),
    sanitizeBody('url').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create video object with escaped and trimmed data.
        var video = new Video({
          title: req.body.title,
          url: req.body.url
        });

        if (!errors.isEmpty()) {
            // There are errors.
            res.send('Error');
            return;
        }
        else {
            // Data from form is valid.
        		video.save(function (err) {
          		if (err) { return next(err); }
          		// Item saved. Return item in json format.
          		res.json(video);
        		});
        }
    }
];

// Handle Video delete on POST.
exports.video_delete = function(req, res, next) {
    // Delete object
    Video.findOneAndDelete({ _id: req.body.videoid }, function deleteVideo(err, video) {
        if (err) { return next(err); }
        // Success
        Videolike.deleteMany({ video: video._id }, function (err) {});
        Videocomment.deleteMany({ video: video._id }, function (err) {});
        res.json(video);
    })
};

// Get likes and comments of a video
exports.video_info = function(req, res, next) {

    async.parallel({
        video: function(callback) {
          Video.findById(req.params.id)
          .exec(callback)
        },
        likes: function(callback) {
          Videolike.countDocuments({ 'video': req.params.id })
          .exec(callback)
        },
        userlike: function(callback) {
          var userid = null;
          if(req.user) {
            userid = req.user._id;
          }
          Videolike.find({ 'video': req.params.id, 'user': userid })
          .exec(callback)
        }
    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.video==null) { // No results.
            var err = new Error('Video not found');
            err.status = 404;
            return next(err);
        }
        // Successful
        res.json(results);
    });

};

// Handle Videolike create on POST.
exports.videolike_create =  [

    // Validate fields
    body('video', 'Video required').isLength({ min: 1 }).trim(),

    // Sanitize fields
    sanitizeBody('video').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create videolike object with escaped and trimmed data.
        var videolike = new Videolike({
          video: req.body.video,
          user: req.user._id
        });

        if (!errors.isEmpty()) {
            // There are errors.
            res.send('Error');
            return;
        }
        else {
            // Data is valid.
            videolike.save(function (err) {
              if (err) { return next(err); }
              // Item saved. Return item in json format.
              res.json({ videolikeid: videolike._id });
            });
        }
    }
];

// Handle Video delete on POST.
exports.videolike_delete = function(req, res, next) {
    // Delete object
    Videolike.findOneAndDelete({ _id: req.body.videolikeid }, function deleteVideolike(err) {
        if (err) { return next(err); }
        // Success
        res.json({ videolikeid: '' });
    })
};

// Display list of all Items.
exports.video_comments = function(req, res, next) {

    async.parallel({
        video: function(callback) {
          Video.findById(req.params.id)
          .exec(callback)
        },
        comments: function(callback) {
          Videocomment.find({ 'video': req.params.id })
          .populate('user', 'local.username')
          .exec(callback)
        },
    }, function(err, results) {
        if (err) { return next(err); } // Error in API usage.
        if (results.video==null) { // No results.
            var err = new Error('Video not found');
            err.status = 404;
            return next(err);
        }
        // Successful
        res.json(results);
    });

};

// Handle Videocomment create on POST.
exports.videocomment_create =  [

    // Validate fields
    body('video', 'Video required').isLength({ min: 1 }).trim(),
    body('comment', 'Comment required').isLength({ min: 1 }).trim(),

    // Sanitize fields
    sanitizeBody('video').trim().escape(),
    sanitizeBody('comment').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create videocomment object with escaped and trimmed data.
        var videocomment = new Videocomment({
          video: req.body.video,
          comment: req.body.comment,
          user: req.user._id
        });

        if (!errors.isEmpty()) {
          console.log(errors);
            // There are errors.
            res.send('Error');
            return;
        }
        else {
            // Data is valid.
            videocomment.save(function (err, comment) {
              if (err) { return next(err); }
              // Item saved. Return item in json format.
              Videocomment.populate(comment, { path: 'user', select: 'local.username' }, function (err, comment) {
                res.json(comment);
              });
            });
        }
    }
];

// Handle Video delete on POST.
exports.videocomment_delete = function(req, res, next) {
    // Delete object
    Videocomment.findOneAndDelete({ _id: req.body.comment }, function deleteVideocomment(err) {
        if (err) { return next(err); }
        // Success
        res.json({ comment: 'deleted' });
    })
};
